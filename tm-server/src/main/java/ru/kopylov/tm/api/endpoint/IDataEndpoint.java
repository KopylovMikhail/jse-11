package ru.kopylov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IDataEndpoint {

    @NotNull
    String getUrl();

    @WebMethod
    void saveDataBin(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void loadDataBin(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void saveDataJson(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void loadDataJson(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void saveDataJsonJaxb(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void loadDataJsonJaxb(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void saveDataXml(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void loadDataXml(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void saveDataXmlJaxb(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void loadDataXmlJaxb(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

}
