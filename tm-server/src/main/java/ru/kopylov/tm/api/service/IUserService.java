package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.User;

import java.util.List;

public interface IUserService {

    boolean merge(@Nullable User user);

    boolean merge(
            @NotNull String currentUserId,
            @NotNull String login,
            @NotNull String password
    );

    boolean persist(@Nullable User user);

    boolean persist(@NotNull String login, @NotNull String password);

    @Nullable
    User findOne(@Nullable String id);

    @Nullable User findOne(@NotNull String login, @NotNull String password);

    @NotNull
    List<User> findAll();

}
