package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IDataService getDataService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    IPropertyService getPropertyService();

}
