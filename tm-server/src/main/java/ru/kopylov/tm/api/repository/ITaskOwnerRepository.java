package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ITaskOwnerRepository {

    void persist(@NotNull String projectId);

    void merge(@NotNull String projectId, @NotNull String taskId);

    @NotNull
    List<String> findAllByProjectId(@NotNull String projectId);

    @NotNull
    List<String> findAll();

    void removeTask(@NotNull String taskId);

}
