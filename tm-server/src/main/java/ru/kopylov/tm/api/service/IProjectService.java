package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;

import java.text.ParseException;
import java.util.List;

public interface IProjectService {

    boolean persist(@Nullable Project project);

    boolean persist(@Nullable String currentUserId, @Nullable String projectName);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(String currentUserId);

    @Nullable
    List<Project> findAll(@Nullable String currentUserId, @Nullable String typeSort);

    boolean merge(@Nullable Project project);

    boolean merge(
            @Nullable String currentUserId,
            @NotNull Integer projectNumber,
            @Nullable String projectName,
            @Nullable String projectDescription,
            @Nullable String projectDateStart,
            @Nullable String projectDateFinish,
            @Nullable Integer stateNumber
    ) throws ParseException;

    boolean remove(@Nullable String projectId);

    boolean remove(@Nullable String currentUserId, @NotNull Integer projectNumber);

    void removeAll();

    void removeAll(@Nullable String currentUserId);

    boolean setTask(
            @Nullable String currentUserId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    boolean setTask(
            @Nullable String currentUserId,
            @NotNull Integer projectNumber,
            @NotNull Integer taskNumber
    );

    @NotNull
    List<Task> getTaskList(@Nullable String currentUserId, @Nullable String projectName);

    @NotNull
    List<Task> getTaskList(@Nullable String currentUserId, @NotNull Integer projectNumber);

    @NotNull
    List<Project> findByContent(@Nullable String content);

}
