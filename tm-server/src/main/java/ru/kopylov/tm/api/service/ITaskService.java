package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Task;

import java.text.ParseException;
import java.util.List;

public interface ITaskService {

    boolean persist(@Nullable Task task);

    boolean persist(@Nullable String currentUserId, @Nullable String taskName);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable String currentUserId);

    @Nullable
    List<Task> findAll(@Nullable String currentUserId, @Nullable String typeSort);

    boolean merge(@Nullable Task task);

    boolean merge(
            @Nullable String currentUserId,
            @NotNull Integer taskNumber,
            @Nullable String taskName,
            @Nullable String taskDescription,
            @Nullable String taskDateStart,
            @Nullable String taskDateFinish,
            @Nullable Integer stateNumber
    ) throws ParseException;

    boolean remove(@Nullable String taskId);

    boolean remove(@Nullable String currentUserId, @NotNull Integer taskNumber);

    void removeAll();

    void removeAll(@Nullable String currentUserId);

    @NotNull
    List<Task> findByContent(@Nullable String content);

}
