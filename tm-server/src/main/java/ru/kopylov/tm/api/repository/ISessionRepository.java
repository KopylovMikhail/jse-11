package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Session;

public interface ISessionRepository {

    boolean remove(@NotNull String sessionId);

    @Nullable
    Session persist(@NotNull final Session session);

    @Nullable
    Session findOne(@NotNull final String sessionId);

}
