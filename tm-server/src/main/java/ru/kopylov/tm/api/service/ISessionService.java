package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;

public interface ISessionService {

    boolean remove(@Nullable String sessionId);

    boolean persist(@Nullable Session session);

    @Nullable Session persist(@Nullable User user);

    Session findOne(@Nullable String sessionId);

    void validate(@Nullable final Session session) throws Exception;

}
