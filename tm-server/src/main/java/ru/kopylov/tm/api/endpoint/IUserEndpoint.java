package ru.kopylov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint {

    @NotNull
    String getUrl();

    @WebMethod
    boolean persistUser(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    );

    @Nullable
    @WebMethod
    Session loginUser(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    );

    @WebMethod
    void logoutUser(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    public User getUserProfile(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    public void updateUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

}
