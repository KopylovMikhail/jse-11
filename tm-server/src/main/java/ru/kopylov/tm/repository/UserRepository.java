package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository implements IUserRepository {

    @NotNull
    private final static Map<String, User> userMap = new LinkedHashMap<>();

    public void merge(@NotNull final User user) {
        userMap.put(user.getId(), user);
    }

    @Nullable
    public User persist(@NotNull final User user) {
        return userMap.putIfAbsent(user.getId(), user);
    }

    @Nullable
    public User findOne(@NotNull final String id) {
        return userMap.get(id);
    }

    public boolean remove(@NotNull final String id) {
        return userMap.entrySet()
                .removeIf(entry -> id.equals(entry.getKey()));
    }

    @NotNull
    public List<User> findAll() {
        return new ArrayList<>(userMap.values());
    }

}
