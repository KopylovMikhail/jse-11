package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ISessionRepository;
import ru.kopylov.tm.entity.Session;

import java.util.LinkedHashMap;
import java.util.Map;

@NoArgsConstructor
public final class SessionRepository extends AbstractRepository implements ISessionRepository {

    @NotNull
    private final static Map<String, Session> sessionMap = new LinkedHashMap<>();

    @Override
    public boolean remove(@NotNull final String sessionId) {
        return sessionMap.entrySet()
                .removeIf(entry -> sessionId.equals(entry.getKey()));
    }

    @Override
    @Nullable
    public Session persist(@NotNull final Session session) {
        return sessionMap.putIfAbsent(session.getId(), session);
    }

    @Nullable
    public Session findOne(@NotNull final String sessionId) {
        return sessionMap.get(sessionId);
    }

}
