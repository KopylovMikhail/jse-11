package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.util.HashUtil;

import java.util.List;

@NoArgsConstructor
public final class UserService extends AbstractService implements IUserService {

    @NotNull
    private IUserRepository userRepository = (IUserRepository) abstractRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean merge(@Nullable final User user) {
        if (user == null) return false;
        if (user.getId() == null || user.getId().isEmpty()) return false;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return false;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return false;
        if (userRepository.findOne(user.getId()) == null) return false;
        userRepository.merge(user);
        return true;
    }

    public boolean merge(
            @NotNull final String currentUserId,
            @NotNull final String login,
            @NotNull final String password
    ) {
        @Nullable final User user = findOne(currentUserId);
        if (user == null) return false;
        @NotNull final String hashPassword = HashUtil.hash(password);
        user.setLogin(login);
        user.setPassword(hashPassword);
        userRepository.merge(user);
        return true;
    }

    public boolean persist(@Nullable final User user) {
        if (user == null) return false;
        return !user.equals(userRepository.persist(user));
    }

    public boolean persist(@NotNull final String login, @NotNull final String password) {
        if (password.isEmpty() || login.isEmpty()) return false;
        @NotNull final List<User> userList = findAll();
        for (@NotNull final User usr : userList) {
            if (login.equals(usr.getLogin())) return false;
        }
        @NotNull final String hashPassword = HashUtil.hash(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(hashPassword);
        user.setRole(TypeRole.USER);
        return !user.equals(userRepository.persist(user));
    }

    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findOne(id);
    }

    @Nullable
    public User findOne(@NotNull final String login, @NotNull final String password) {
        if (password.isEmpty() || login.isEmpty()) return null;
        @NotNull final String hashPassword = HashUtil.hash(password);
        @Nullable User loggedUser = null;
        @NotNull final List<User> userList = findAll();
        for (@NotNull final User user : userList) {
            if (login.equals(user.getLogin()) && hashPassword.equals(user.getPassword())) {
                loggedUser = user;
                break;
            }
        }
        return loggedUser;
    }

    @NotNull
    public List<User> findAll() {
        return userRepository.findAll();
    }

}
