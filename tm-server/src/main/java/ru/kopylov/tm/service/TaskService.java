package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ITaskOwnerRepository;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.enumerated.TypeSort;
import ru.kopylov.tm.util.DateUtil;
import ru.kopylov.tm.util.TaskComparator;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {

    @NotNull
    private ITaskRepository taskRepository = (ITaskRepository) abstractRepository;

    @NotNull
    private ITaskOwnerRepository taskOwnerRepository = (ITaskOwnerRepository) abstractRepository;

    public TaskService(
            @NotNull final ITaskRepository taskRepository,
            @NotNull final ITaskOwnerRepository taskOwnerRepository
    ) {
        this.taskRepository = taskRepository;
        this.taskOwnerRepository = taskOwnerRepository;
    }

    public boolean persist(@Nullable final Task task) {
        if (task == null ) return false;
        return !task.equals(taskRepository.persist(task));
    }

    public boolean persist(@Nullable final String currentUserId, @Nullable final String taskName) {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setUserId(currentUserId);
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        return !task.equals(taskRepository.persist(task));
    }

    @NotNull
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    public List<Task> findAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAll(currentUserId);
    }

    @Nullable
    public List<Task> findAll(@Nullable final String currentUserId, @Nullable final String typeSort) {
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        @NotNull final List<Task> taskList = taskRepository.findAll(currentUserId);
        if (TypeSort.CREATE_DATE.getDisplayName().equals(typeSort) || typeSort == null || typeSort.isEmpty())
            return taskList;
        if (TypeSort.START_DATE.getDisplayName().equals(typeSort)) {
            taskList.sort(TaskComparator.byDateStart);
            return taskList;
        }
        if (TypeSort.FINISH_DATE.getDisplayName().equals(typeSort)) {
            taskList.sort(TaskComparator.byDateFinish);
            return taskList;
        }
        if (TypeSort.STATE.getDisplayName().equals(typeSort)) {
            taskList.sort(TaskComparator.byState);
            return taskList;
        }
        return null;
    }

    public boolean merge(@Nullable final Task task) {
        if (task == null ) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        if (taskRepository.findOne(task.getId()) == null) return false;
        taskRepository.merge(task);
        return true;
    }

    public boolean merge(
            @Nullable final String currentUserId,
            @NotNull final Integer taskNumber,
            @Nullable final String taskName,
            @Nullable final String taskDescription,
            @Nullable final String taskDateStart,
            @Nullable final String taskDateFinish,
            @Nullable final Integer stateNumber
    ) throws ParseException {
        @NotNull final List<Task> taskList = taskRepository.findAll(currentUserId);
        if (taskNumber < 1 || taskNumber > taskList.size()) return false;
        @NotNull final Task task = taskList.get(taskNumber - 1);
        if (taskName != null && !taskName.isEmpty()) task.setName(taskName);
        if (taskDescription != null && !taskDescription.isEmpty())
            task.setDescription(taskDescription);
        if (taskDateStart != null && !taskDateStart.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.stringToDate(taskDateStart);
            task.setDateStart(dateStart);
        }
        if (taskDateFinish != null && !taskDateFinish.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.stringToDate(taskDateFinish);
            task.setDateFinish(dateFinish);
        }
        if (stateNumber != null) {
            if (stateNumber < 1 || stateNumber > State.values().length) return false;
            task.setState(State.values()[stateNumber-1]);
        }
        return merge(task);
    }

    public boolean remove(@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) return false;
        taskOwnerRepository.removeTask(taskId);
        return taskRepository.remove(taskId);
    }

    public boolean remove(@Nullable final String currentUserId, @NotNull final Integer taskNumber) {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Task> taskList = taskRepository.findAll(currentUserId);
        if (taskNumber < 1 || taskNumber > taskList.size()) return false;
        @NotNull final Task task = taskList.get(taskNumber - 1);
        return remove(task.getId());
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public void removeAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        @NotNull final List<Task> tasks = taskRepository.findAll(currentUserId);
        for (Task task : tasks) {
            if (task.getId() != null)
            taskOwnerRepository.removeTask(task.getId());
        }
        taskRepository.removeAll(currentUserId);
    }

    @NotNull
    public List<Task> findByContent(@Nullable String content) {
        if (content == null || content.isEmpty()) return Collections.emptyList();
        return taskRepository.findByContent(content);
    }

}
