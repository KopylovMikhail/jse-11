package ru.kopylov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.service.IDataService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.constant.DataPath;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Root;
import ru.kopylov.tm.entity.Task;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.util.List;

@NoArgsConstructor
public class DataService implements IDataService {

    private ServiceLocator bootstrap;

    public DataService(@NotNull final ServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void saveDataBin(@Nullable final String userId) throws Exception {
        @NotNull final List<Project> projects = bootstrap.getProjectService().findAll(userId);
        @NotNull final List<Task> tasks = bootstrap.getTaskService().findAll(userId);
        @NotNull final File file = new File(DataPath.PATH_BIN);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(projects);
        objectOutputStream.writeObject(tasks);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    public void loadDataBin() throws Exception {
        @NotNull final File file = new File(DataPath.PATH_BIN);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(file);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        loadProjects(objectInputStream.readObject());
        loadTasks(objectInputStream.readObject());
        objectInputStream.close();
        fileInputStream.close();
    }

    public void saveDataJson(@Nullable final String userId) throws Exception {
        @NotNull final List<Project> projects = bootstrap.getProjectService().findAll(userId);
        @NotNull final List<Task> tasks = bootstrap.getTaskService().findAll(userId);
        @NotNull final Root root = new Root();
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_JSON);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, root);
    }

    public void loadDataJson() throws Exception {
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_JSON);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Root root = objectMapper.readValue(file, Root.class);
        for (@NotNull final Project project : root.getProjects()) {
            bootstrap.getProjectService().persist(project);
        }
        for (@NotNull final Task task : root.getTasks()) {
            bootstrap.getTaskService().persist(task);
        }
    }

    public void saveDataJsonJaxb(@Nullable final String userId) throws Exception {
        @NotNull final List<Project> projects = bootstrap.getProjectService().findAll(userId);
        @NotNull final List<Task> tasks = bootstrap.getTaskService().findAll(userId);
        @NotNull final Root root = new Root();
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_JAXB_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(root, file);
    }

    public void loadDataJsonJaxb() throws Exception {
        @NotNull final File file = new File(DataPath.PATH_JAXB_JSON);
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Root root = (Root) unmarshaller.unmarshal(file);
        for (@NotNull final Project project : root.getProjects()) {
            bootstrap.getProjectService().persist(project);
        }
        for (@NotNull final Task task : root.getTasks()) {
            bootstrap.getTaskService().persist(task);
        }
    }

    public void saveDataXml(@Nullable final String userId) throws Exception {
        @NotNull final List<Project> projects = bootstrap.getProjectService().findAll(userId);
        @NotNull final List<Task> tasks = bootstrap.getTaskService().findAll(userId);
        @NotNull final Root root = new Root();
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_XML);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, root);
    }

    public void loadDataXml() throws Exception {
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_XML);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Root root = xmlMapper.readValue(file, Root.class);
        for (@NotNull final Project project : root.getProjects()) {
            bootstrap.getProjectService().persist(project);
        }
        for (@NotNull final Task task : root.getTasks()) {
            bootstrap.getTaskService().persist(task);
        }
    }

    public void saveDataXmlJaxb(@Nullable final String userId) throws Exception {
        @NotNull final List<Project> projects = bootstrap.getProjectService().findAll(userId);
        @NotNull final List<Task> tasks = bootstrap.getTaskService().findAll(userId);
        @NotNull final Root root = new Root();
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_JAXB_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(root, file);
    }

    public void loadDataXmlJaxb() throws Exception {
        @NotNull final File file = new File(DataPath.PATH_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Root root = (Root) unmarshaller.unmarshal(file);
        for (@NotNull final Project project : root.getProjects()) {
            bootstrap.getProjectService().persist(project);
        }
        for (@NotNull final Task task : root.getTasks()) {
            bootstrap.getTaskService().persist(task);
        }
    }

    private void loadProjects(@Nullable final Object object) {
        if (!(object instanceof List<?>)) return;
        if (((List) object).isEmpty()) return;
        if (!(((List) object).get(0) instanceof Project)) return; //если первый объект листа не принадлежит типу Project, то выход из метода
        @NotNull final List<Project> projects = (List<Project>) object;
        for (@NotNull final Project project : projects) {
            bootstrap.getProjectService().persist(project);
        }
    }

    private void loadTasks(@Nullable final Object object) {
        if (!(object instanceof List<?>)) return;
        if (((List) object).isEmpty()) return;
        if (!(((List) object).get(0) instanceof Task))
            return; //если первый объект листа не принадлежит типу Task, то выход из метода
        @NotNull final List<Task> tasks = (List<Task>) object;
        for (@NotNull final Task task : tasks) {
            bootstrap.getTaskService().persist(task);
        }
    }

}
