package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@NoArgsConstructor
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final Properties properties = new Properties();

    public void init() throws IOException {
        @Nullable final InputStream inputStream = PropertyService.class.getClassLoader()
                .getResourceAsStream("application.properties");
        properties.load(inputStream);
    }

    @NotNull
    public final String getPort() {
        return properties.getProperty("port");
    }

    @NotNull
    public final String getHost() {
        return properties.getProperty("host");
    }

    @NotNull
    public final String getSalt() {
        return properties.getProperty("salt");
    }

    public final int getCycle() {
        return Integer.parseInt(properties.getProperty("cycle"));
    }

    public final long getLifeTime() {
        return Long.parseLong(properties.getProperty("sessionLifeTime"));
    }

}
