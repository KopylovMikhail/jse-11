package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.endpoint.ITaskEndpoint;
import ru.kopylov.tm.api.service.ISessionService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@WebService
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final String url = primaryUrl + this.getClass().getSimpleName() + "?wsdl";

    public TaskEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final ServiceLocator bootstrap
    ) {
        super(sessionService, bootstrap);
    }

    @WebMethod
    public void clearTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        bootstrap.getTaskService().removeAll(session.getUserId());
    }

    @WebMethod
    public boolean createTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getTaskService().persist(session.getUserId(), taskName);
    }

    @NotNull
    @WebMethod
    public List<Task> findTaskContent(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "findWord") @Nullable final String findWord
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getTaskService().findByContent(findWord);
    }

    @Nullable
    @WebMethod
    public List<Task> getTaskList(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "typeSort") @Nullable final String typeSort
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getTaskService().findAll(session.getUserId(), typeSort);
    }

    @WebMethod
    public boolean removeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskNumber") @NotNull final Integer taskNumber
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getTaskService().remove(session.getUserId(), taskNumber);
    }

    @WebMethod
    public boolean updateTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskNumber") @NotNull final Integer taskNumber,
            @WebParam(name = "taskName") @Nullable final String taskName,
            @WebParam(name = "taskDescription") @Nullable final String taskDescription,
            @WebParam(name = "taskDateStart") @Nullable final String taskDateStart,
            @WebParam(name = "taskDateFinish") @Nullable final String taskDateFinish,
            @WebParam(name = "stateNumber") @Nullable final Integer stateNumber
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getTaskService().merge(
                session.getUserId(),
                taskNumber,
                taskName,
                taskDescription,
                taskDateStart,
                taskDateFinish,
                stateNumber
        );
    }

}
