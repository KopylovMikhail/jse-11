package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.endpoint.IUserEndpoint;
import ru.kopylov.tm.api.service.ISessionService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@WebService
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private final String url = primaryUrl + this.getClass().getSimpleName() + "?wsdl";

    public UserEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final ServiceLocator bootstrap) {
        super(sessionService, bootstrap);
    }

    @WebMethod
    public boolean persistUser(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) {
        return bootstrap.getUserService().persist(login, password);
    }

    @Nullable
    @WebMethod
    public Session loginUser(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) {
        @Nullable User loggedUser = bootstrap.getUserService().findOne(login, password);
        return bootstrap.getSessionService().persist(loggedUser);
    }

    @WebMethod
    public void logoutUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        bootstrap.getSessionService().remove(session.getId());
    }

    @Nullable
    @WebMethod
    public User getUserProfile(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getUserService().findOne(session.getUserId());
    }

    @WebMethod
    public void updateUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        bootstrap.getUserService().merge(session.getUserId(), login, password);
    }

}
