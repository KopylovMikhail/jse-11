package ru.kopylov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum TypeSort {

    CREATE_DATE("create-date"),
    START_DATE("start-date"),
    FINISH_DATE("finish-date"),
    STATE("state");

    @NotNull
    private final String displayName;

    TypeSort(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
