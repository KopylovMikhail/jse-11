package ru.kopylov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.ITerminalService;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.endpoint.*;
import ru.kopylov.tm.service.TerminalService;

import java.lang.Exception;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable
    private Session session = null;

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpointService().getDataEndpointPort();

    public void init(@NotNull Set<Class<? extends AbstractCommand>> classes) throws Exception {
        try {
            for (@NotNull Class clazz : classes) {
                registryCommand(clazz);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        @Nullable String command = "";
        do {
            try {
                command = terminalService.getReadLine();
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                if (e.getMessage() == null) System.out.println(e.toString());
            }
        } while (!"exit".equals(command));
    }

    private void registryCommand(@NotNull Class clazz) throws Exception {
        if (AbstractCommand.class.isAssignableFrom(clazz)) {
            @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setBootstrap(this);
            commands.put(command.getName(), command);
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

}
