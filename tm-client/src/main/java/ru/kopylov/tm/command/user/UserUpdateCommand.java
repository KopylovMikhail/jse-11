package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.endpoint.Session;

@NoArgsConstructor
public final class UserUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update user password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER PASSWORD UPDATE]\n" +
                "ENTER NEW LOGIN:");
        @Nullable final String newLogin = bootstrap.getTerminalService().getReadLine();
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String newPassword = bootstrap.getTerminalService().getReadLine();
        if (newLogin == null || newLogin.isEmpty() || newPassword == null || newPassword.isEmpty()) {
            System.out.println("LOGIN OR PASSWORD IS EMPTY.\n");
            return;
        }
        @Nullable Session session = bootstrap.getSession();
        bootstrap.getUserEndpoint().updateUser(session, newLogin, newPassword);
        System.out.println("[PASSWORD UPDATE SUCCESS]\n");
    }

}
