package ru.kopylov.tm.command.data.binary;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class SaveSerializeCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-bin-save";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saving a subject area using serialization.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN SAVE]");
        bootstrap.getDataEndpoint().saveDataBin(bootstrap.getSession());
        System.out.println("[OK]");
    }

}
